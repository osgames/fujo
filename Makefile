zip:
	zip -r -9 --exclude='*.pyc' fujo.zip fujo/ simplejson/ __main__.py
	cat hashbang fujo.zip > fujo.zip.py
	chmod +x fujo.zip.py

windows:
	python setup.py py2exe
