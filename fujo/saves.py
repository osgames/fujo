#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import simplejson


SAVE_FILE = '~/.fujo.sav'


class Saveable(object):
    # kinds = {}  # Each type of saveable needs its own dict of kinds

    @classmethod
    def register(cls, kind):
        cls.kinds[kind.__name__] = kind
        return kind

    @classmethod
    def create(cls, data, *args, **kwargs):
        kind = cls.kinds[data['kind']]
        mob = kind(*args, **kwargs)
        mob.load(data)
        return mob

    def save(self):
        return {
            'kind': self.__class__.__name__,
        }

    def load(self, data):
        pass


def save(level_data):
    filename = os.path.expanduser(SAVE_FILE)
    data = {
        'version': 3,
        'level': level_data,
    }
    with open(filename, 'wb') as f:
        simplejson.dump(data, f, sort_keys=True, indent=' ')


def load():
    filename = os.path.expanduser(SAVE_FILE)
    with open(filename, 'rb') as f:
        data = simplejson.load(f)
    if data['version'] != 3:
        raise ValueError('Bad save version %r' % data['version'])
    return data['level']


def valid():
    try:
        load()
    except Exception:
        return False
    return True


def delete():
    filename = os.path.expanduser(SAVE_FILE)
    try:
        os.remove(filename)
    except IOError:
        pass
