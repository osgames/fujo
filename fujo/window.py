#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame

from .resources import RES


def outlined_text(text, color, border_color):
    notcolor = [c^0xFF for c in border_color]
    base = RES.font.render(text, 0, border_color, notcolor)
    size = base.get_width() + 2, base.get_height() + 2
    image = pygame.Surface(size, 16)
    image.fill(notcolor)
    base.set_colorkey(0)
    image.blit(base, (0, 0))
    image.blit(base, (2, 0))
    image.blit(base, (0, 2))
    image.blit(base, (2, 2))
    base.set_colorkey(0)
    base.set_palette_at(1, color)
    image.blit(base, (1, 1))
    image.set_colorkey(notcolor)
    return image


class Window(pygame.sprite.Sprite):
    def __init__(self, rect):
        super(Window, self).__init__()
        self.rect = pygame.Rect(rect)
        self.image = pygame.Surface(self.rect.size, pygame.SRCALPHA)
        self.render()

    def render(self):
        w, h = self.rect.size
        scale = pygame.transform.scale
        self.image.blit(RES.window[0], (0, 0))
        self.image.blit(scale(RES.window[1], (w - 32, 16)), (16, 0))
        self.image.blit(RES.window[2], (w - 16, 0))
        self.image.blit(scale(RES.window[3], (16, h - 32)), (0, 16))
        self.image.blit(scale(RES.window[4], (w - 32, h - 32)), (16, 16))
        self.image.blit(scale(RES.window[5], (16, h - 32)), (w - 16, 16))
        self.image.blit(RES.window[6], (0, h - 16))
        self.image.blit(scale(RES.window[7], (w - 32, 16)), (16, h - 16))
        self.image.blit(RES.window[8], (w - 16, h - 16))


class Message(Window):
    def __init__(self, rect, lines):
        self.lines = lines
        rect = pygame.Rect(rect)
        rect.height = max(len(lines) * 12 + 22, rect.height)
        super(Message, self).__init__(rect)

    def render(self):
        super(Message, self).render()
        self.background = self.image.copy()
        self.render_lines()

    def render_lines(self):
        for y, line in enumerate(self.lines):
            text = outlined_text(line, (0xee, 0xee, 0xff), (0x00, 0x00, 0x22))
            self.image.blit(text, (8, 8 + y * 12))

    def set_lines(self, lines):
        self.lines = lines
        self.image = self.background.copy()
        self.render_lines()


class Cursor(pygame.sprite.Sprite):
    def __init__(self):
        super(Cursor, self).__init__()
        self.image = RES.window[9]
        self.rect = self.image.get_rect()
        self.dx = 0
        self.ddx = 1
        self.steps = 0

    def update(self):
        if self.steps > 0:
            self.steps -= 1
            self.rect.move_ip(0, 4)
        elif self.steps  < 0:
            self.steps += 1
            self.rect.move_ip(0, -4)
        else:
            self.rect.move_ip(self.dx, 0)
            self.dx += self.ddx
            if abs(self.dx) > 2:
                self.ddx = -self.ddx

class Menu(Message):
    def __init__(self, rect, items, depth=0):
        self.items = items
        lines = [label for value, label in items]
        super(Menu, self).__init__(rect, lines)
        self.cursor = Cursor()
        self.cursor.rect.right = self.rect.left
        self.depth = depth
        self.cursor.depth = self.depth + 1
        self.pos = 0

    def depress(self, display):
        KEYS = [
            pygame.K_UP,
            pygame.K_DOWN,
            pygame.K_RETURN,
            pygame.K_ESCAPE,
        ]
        keys = pygame.key.get_pressed()
        while any(keys[k] for k in KEYS):
            display.update()
            display.tick()
            keys = pygame.key.get_pressed()

    def run(self, display):
        display.windows.add(self)
        self.depress(display)
        display.windows.add(self.cursor)
        self.cursor.rect.top = self.rect.top + 10 + self.pos * 12
        end = len(self.items) - 1
        chosen = None
        while True:
            display.tick()
            if self.cursor.steps == 0:
                keys = pygame.key.get_pressed()
                if keys[pygame.K_UP]:
                    if self.pos > 0:
                        self.cursor.steps -= 3
                        self.pos -= 1
                    else:
                        self.cursor.rect.top = self.rect.top + 10 + end * 12
                        self.cursor.steps = 0
                        self.pos = end
                        self.depress(display)
                elif keys[pygame.K_DOWN]:
                    if self.pos < end:
                        self.cursor.steps += 3
                        self.pos += 1
                    else:
                        self.cursor.rect.top = self.rect.top + 10
                        self.cursor.steps = 0
                        self.pos = 0
                        self.depress(display)
                elif keys[pygame.K_ESCAPE] or display.quit:
                    break
                elif keys[pygame.K_RETURN]:
                    if self.items:
                        chosen = self.items[self.pos][0]
                    break
            display.update()
        self.depress(display)
        self.cursor.kill()
        return chosen


class Label(pygame.sprite.Sprite):
    def __init__(self, place, text, color=(0xee, 0xff, 0xff), delay=0,
                 offset=0):
        super(Label, self).__init__()
        self.image = outlined_text(text, color, (0x00, 0x00, 0x22))
        self.rect = self.image.get_rect()
        self.rect.midbottom = place
        self.depth = self.rect.bottom + 48
        self.rect.move_ip(0, -offset * 12)
        self.speed = 1
        self.delay = delay
        if self.delay:
            self.image.set_alpha(0)
        self.step = 0

    def update(self, display=None):
        if self.delay:
            self.delay -= 1
            return
        if self.step < 4 - self.speed:
            self.step += 1
            return
        self.step = 0
        self.image.set_alpha(255 - self.speed * 12)
        self.rect.move_ip(0, -1)
        self.speed += 1
        if self.speed > 10:
            self.kill()
