#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

from . import mobs
from . import things
from . import items
from . import saves
from .util import dpos


AREAS = [
    ((1, 2), (5, 7)), ((7, 2), (5, 7)), ((15, 2), (5, 7)),
    ((1, 12), (5, 7)), ((7, 12), (5, 7)), ((15, 12), (5, 7)),
    ((1, 22), (5, 7)), ((7, 22), (5, 7)), ((15, 22), (5, 7)),
]
MONSTERS = [
    [mobs.Bat, mobs.Martian],
    [mobs.Bat, mobs.Martian, mobs.Kappa, mobs.Tanuki],
    [mobs.Martian, mobs.Kappa, mobs.Tanuki, mobs.Tengu],
    [mobs.Kappa, mobs.Tengu, mobs.Tanuki, mobs.Oni],
    [mobs.Ghost, mobs.Tengu, mobs.Oni],
]


class Level(saves.Saveable):
    looks = 0
    kinds = {}

    def __init__(self, player=None):
        self.player = player
        self.size = 21, 32
        if player:
            self.depth = player.level
        self.floor = [[0] * self.size[0] for i in xrange(self.size[1])]
        self.walls = [[1] * self.size[0] for i in xrange(self.size[1])]
        self.explored = [[0] * self.size[0] for i in xrange(self.size[1])]
        self.things = {}
        self.mobs = {}
        self.rooms = []
        self.empty = []

    def generate(self):
        rooms = []
        r = random.randint
        for (x, y), (w, h) in AREAS:
            width = r(1, w / 2) * 2
            height = r(1, h / 2) * 2
            x_offset = r(0, (w - width) / 2) * 2
            y_offset = r(0, (h - height) / 2) * 2
            rooms.append(((x + x_offset, y + y_offset), (width, height)))

        random.shuffle(rooms)

        for room, next_room in zip(rooms, rooms[1:]):
            (x, y), (w, h) = room
            (nx, ny), (nw, nh) = next_room
            x1 = nx + r(0, nw / 2) * 2
            y1 = ny + r(0, nh / 2) * 2
            x2 = x + r(0, w / 2) * 2
            y2 = y + r(0, h / 2) * 2
            self._path((x1, y1), (x2, y2))

        for (x, y), (w, h) in rooms:
            self._fill((x, y), (w + 1, h + 1))

        self.rooms = rooms

    def _fill(self, (sx, sy), (width, height)):
        for y in xrange(sy, sy + height):
            for x in xrange(sx, sx + width):
                self.walls[y][x] = 0
                self.floor[y][x] = random.randrange(1, 5)

    def _path(self, (sx, sy), (ex, ey)):
        for x in xrange(sx, ex, 1 if sx < ex else -1):
            if not self.walls[sy][x]:
                return
            self.walls[sy][x] = 0
            if self.floor[sy][x] == 0:
                self.floor[sy][x] = 1
        for y in xrange(sy, ey, 1 if sy < ey else -1):
            if not self.walls[y][ex]:
                return
            self.walls[y][ex] = 0
            if self.floor[y][ex] == 0:
                self.floor[y][ex] = 1

    def place_thing(self, thing, pos):
        thing.set_pos(pos)
        self.things[pos] = thing

    def place_mob(self, mob, pos):
        if mob.pos:
            del self.mobs[mob.pos]
        if pos is None:
            pos = mob.pos
        if not pos:
            raise ValueError("Specify position.")
        if pos in self.mobs:
            raise KeyError("Already occupied.")
        mob.set_pos(pos)
        self.mobs[pos] = mob

    def attack_mob(self, attacker, defender):
        attack = random.randint(0, attacker.attack)
        defense = random.randint(0, defender.defense)
        damage = 0
        if attack > defense:
            damage = random.randint(1, attacker.damage + 1)
        defender.wound(damage)
        for mob in self.mobs.values():
            if mob != attacker:
                mob.mob_attacked(self, defender, damage)

    def mob_mob(self, mob):  # rim shot
        if mob.is_player:
            item = random.choice(mob.items)
            mob.discard_item(item)
        else:
            item = mob.item
        return item

    def pick_item(self, thing):
        for mob in self.mobs.values():
            mob.item_picked(self, thing)
        del self.things[thing.pos]
        thing.pick_up(self)
        return thing

    def drop_item(self, pos, item):
        if self.things.get(pos):
            item.kill()
            return
        self.things[pos] = item
        item.pos = None
        item.set_pos(pos)
        self.player.new_things.append(item)

    def throw_item(self, thrower, item, direction):
        last_pos = thrower.pos
        pos = dpos(thrower.pos, direction)
        item.set_pos(thrower.pos)
        steps = 0
        while not self.is_blocked(pos):
            last_pos = pos
            pos = dpos(pos, direction)
            steps += 1
        mob = self.mobs.get(pos)
        if mob == self:
            mob = None
        if mob:
            mob.item_thrown(self, item)
        drop = not item.throw(self, pos, mob)
        item.animation = item.throw_anim(thrower.pos, steps, direction, drop)
        if drop:
            self.drop_item(last_pos, item)

    def populate(self):
        self.empty = []
        for (sx, sy), (w, h) in self.rooms:
            for y in xrange(sy, sy + h + 1):
                for x in xrange(sx, sx + w + 1):
                    self.empty.append((x, y))
        random.shuffle(self.empty)
        try:
            monsters = MONSTERS[self.depth]
        except IndexError:
            # Final level
            self.looks = 1
            boss = mobs.Boss()
            self.place_mob(boss, self.empty.pop())
            for i in xrange(2):
                item = random.choice(items.FOOD)()
                self.place_thing(item, self.empty.pop())
            for i in xrange(4):
                item = random.choice(items.WEAPONS)()
                self.place_thing(item, self.empty.pop())
            for i in xrange(2):
                item = random.choice(items.ARMOR)()
                self.place_thing(item, self.empty.pop())
        else:
            self.place_thing(things.Ladder(), self.empty.pop())
            for i in xrange(3 + 5 * self.depth):
                Monster = random.choice(monsters)
                self.place_mob(Monster(), self.empty.pop())
            for i in xrange(10 - 2 * self.depth):
                item = random.choice(items.ITEMS)()
                self.place_thing(item, self.empty.pop())
            for i in xrange(2):
                # Guaranteed food.
                item = random.choice(items.FOOD)()
                self.place_thing(item, self.empty.pop())
        for i in xrange(12):
            self.place_thing(random.choice(things.DEBRIS)(), self.empty.pop())
        pos = self.empty.pop()
        self.place_thing(things.Hole(), pos)
        self.place_mob(self.player, pos)

    def is_wall(self, pos):
        return self.walls[pos[1]][pos[0]] or self.mobs.get(pos)

    def is_blocked(self, pos):
        return self.is_wall(pos) or self.mobs.get(pos)

    def is_explored(self, pos):
        return self.explored[pos[1]][pos[0]]

    def explore(self, pos):
        count = 0
        for (rx, ry), (w, h) in self.rooms:
            if rx <= pos[0] <= rx + w and ry <= pos[1] <= ry + h:
                for y in xrange(ry - 1, ry + h + 2):
                    for x in xrange(rx - 1, rx + w + 2):
                        if self.explored[y][x] == 0:
                            count += 1
                        self.explored[y][x] = 2
                break
        for y in xrange(max(0, pos[1] - 1), min(self.size[1], pos[1] + 2)):
            for x in xrange(max(0, pos[0] - 1), min(self.size[1], pos[0] + 2)):
                if self.explored[y][x] == 0:
                    self.explored[y][x] = 1
                    count += 1
        return count

    def save(self):
        data = super(Level, self).save()
        data.update({
            'depth': self.depth,
            'size': self.size,
            'floor': [''.join('#.,:='[f] for f in row)
                      for row in self.floor],
            'walls': [''.join(' #'[w] for w in row) for row in self.walls],
            'explored': [''.join(' xX'[x] for x in row)
                         for row in self.explored],
            'looks': self.looks,
            'rooms': self.rooms,
            'empty': self.empty,
            'mobs': [mob.save() for mob in self.mobs.values()],
            'things': [thing.save() for thing in self.things.values()],
            'identified': [s.__name__ for s in items.SPELLS if s.identified]
        })
        return data

    def load(self, data):
        super(Level, self).load(data)
        self.depth = data['depth']
        self.size = data['size']
        FLOOR = {'#': 0, '.': 1, ',': 2, ':': 3, '=': 4}
        self.floor = [[FLOOR[f] for f in row] for row in data['floor']]
        WALL = {' ': 0, '#': 1}
        self.walls = [[WALL[w] for w in row] for row in data['walls']]
        EXP = {' ': 0, 'x': 1, 'X': 2}
        self.explored = [[EXP[x] for x in row] for row in data['explored']]
        self.looks = data['looks']
        self.rooms = [tuple(room) for room in data['rooms']]
        self.empty = [tuple(empty) for empty in data['empty']]
        mob_list = [mobs.Mob.create(mob) for mob in data['mobs']]
        self.mobs = dict((mob.pos, mob) for mob in mob_list)
        for mob in mob_list:
            if mob.is_player:
                self.player = mob
                break
        things_list = [things.Thing.create(thing) for thing in data['things']]
        self.things = dict((thing.pos, thing) for thing in things_list)
        for spell in items.SPELLS:
            spell.identified = spell.__name__ in data['identified']

@Level.register
class BasicLevel(Level):
    pass
