#!/usr/bin/env python
# -*- coding: utf-8 -*-

import StringIO
import pkgutil
import tempfile
import os

import pygame




def load_data(filename):
    return pkgutil.get_data('fujo', filename)


def load_file(filename):
    return StringIO.StringIO(load_data(filename))




def load_image(filename):
    return pygame.image.load(load_file(filename), filename)


def load_atlas(filename, width=32, height=None):
    image = load_image(filename)
    image_width, image_height = image.get_size()
    w, h = width, height or width
    return [
        image.subsurface((x * w, y * h, w, h))
        for y in xrange(0, image_height // h)
        for x in xrange(0, image_width // w)
    ]


def load_font(filename, size=8):
    # For some reason we need to have a real file on Windows.
    tmpdir = tempfile.mkdtemp()
    fname = os.path.join(tmpdir, filename)
    try:
        with open(fname, 'wb') as f:
            f.write(load_data(filename))
        font = pygame.font.Font(fname, size)
    finally:
        try:
            os.remove(fname)
            os.rmdir(tmpdir)
        except:
            pass
    return font


class Resources(object):
    def load(self):
        self.wall_tiles = load_atlas("walls.png", 24, 16)
        self.floor_tiles = load_atlas("floors.png", 24, 16)
        self.font = load_font("pf_ronda_seven_bold.ttf")
        self.player = load_atlas("player.png", 32, 32)
        self.monsters = load_atlas("monsters.png", 32, 32)
        self.things = load_atlas("things.png", 32, 32)
        self.window = load_atlas("window.png", 16, 16)
        self.title = load_image("title.png")

        for i in range(4):
            # Make the ghosts translucent
            self.monsters[i * 8 + 5].set_alpha(125)
            # Make the jellies translucent
            self.monsters[i * 8 + 0].set_alpha(200)
            self.monsters[i * 8 + 1].set_alpha(200)

    def load_music(self):
        filename = "music.ogg"
        pygame.mixer.music.load(load_file(filename))

RES = Resources()
