#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

from . import saves
from .util import dpos, dist, dist2


class Behavior(saves.Saveable):
    kinds = {}
    priority = 0

    def __init__(self):
        self.mob = None

    def added(self):
        """Called when the monster starts using the behavior."""

    def act(self, level):
        """Called when it's the monster's turn to act."""

    def mob_attacked(self, level, mob, damage):
        """Called when any mob on the level is attacked."""

    def item_thrown(self, level, item):
        """Called when the monster is hit with a thrown item."""

    def item_picked(self, level, thing):
        """Called when any item on the level is picked up."""

    def wound(self, damage):
        """Called when this mob is attacked."""


@Behavior.register
class Wander(Behavior):
    def act(self, level):
        possible = [d for d in xrange(8)
                    if not level.is_blocked(dpos(self.mob.pos, d))]
        if possible:
            random.shuffle(possible)
            d = possible[0]
            self.mob.do_walk(level, d)
            return True


@Behavior.register
class Confused(Wander):
    priority = 200

    def __init__(self, confused=None):
        super(Confused, self).__init__()
        self.confused = confused

    def act(self, level):
        self.mob.label('???')
        self.confused  -= 1
        if self.confused <= 0:
            self.mob.del_behavior(self)
        return super(Confused, self).act(level)

    def item_picked(self, level, thing):
        return True

    def mob_attacked(self, level, mob, damage):
        return True

    def wound(self, damage):
        self.mob.del_behavior(self)

    def save(self):
        data = super(Confused, self).save()
        data['confused'] = self.confused
        return data

    def load(self, data):
        super(Confused, self).load(data)
        self.confused = data['confused']


@Behavior.register
class Hide(Behavior):
    priority = 200

    def added(self):
        self.mob.animation = self.mob.hide_anim()

    def act(self, level):
        # Do nothing.
        return True

    def item_picked(self, level, thing):
        return True

    def mob_attacked(self, level, mob, damage):
        if mob == self.mob:
            self.mob.del_behavior(self)
            level.player.smoke(self.mob.rect.midbottom,12)
        return True

    def wound(self, damage):
        return True

    def item_thrown(self, level, item):
        self.mob.del_behavior(self)
        level.player.smoke(self.mob.rect.midbottom,12)


@Behavior.register
class Sleep(Behavior):
    priority = 151

    def __init__(self, asleep=None):
        super(Sleep, self).__init__()
        self.asleep = asleep

    def act(self, level):
        if level.is_explored(self.mob.pos):
            self.mob.label('.zZ')
        if self.asleep is None:
            return True
        if dist(self.mob.pos, level.player.pos) < 6:
            self.asleep  -= 1
        if self.asleep <= 0:
            self.mob.del_behavior(self)
        return True

    def wound(self, damage):
        self.mob.del_behavior(self)

    def item_thrown(self, level, item):
        self.mob.del_behavior(self)

    def item_picked(self, level, thing):
        return True

    def mob_attacked(self, level, mob, damage):
        return True

    def save(self):
        data = super(Sleep, self).save()
        data['asleep'] = self.asleep
        return data

    def load(self, data):
        super(Sleep, self).load(data)
        self.asleep = data['asleep']


@Behavior.register
class Hostile(Behavior):
    priority = 150

    def act(self, level):
        for d in xrange(8):
            if level.player.pos == dpos(self.mob.pos, d):
                self.mob.do_attack(level, d, level.player)
                return True


@Behavior.register
class Excitable(Behavior):
    def __init__(self):
        super(Excitable, self).__init__()
        self.raging = False

    def _rage(self):
        if not self.raging:
            self.mob.label('*$@%&!')
            self.raging = True
        self.mob.add_behavior(Rage(10))

    def item_picked(self, level, thing):
        # Picking up items makes nearby monsters angry
        if dist(self.mob.pos, thing.pos) < 6:
            self._rage()

    def mob_attacked(self, level, mob, damage):
        # Attacking monsters makes nearby monsters angry
        if dist(self.mob.pos, mob.pos) < 6:
            self._rage()

    def item_thrown(self, level, item):
        self._rage()

    def wound(self, damage=0):
        self._rage()

    def save(self):
        data = super(Excitable, self).save()
        data['raging'] = self.raging
        return data

    def load(self, data):
        super(Excitable, self).load(data)
        self.raging = data['raging']


@Behavior.register
class Coward(Behavior):
    def mob_attacked(self, level, mob, damage):
        if dist(self.mob.pos, mob.pos) < 6:
            self.mob.add_behavior(Panic(10))

    def item_thrown(self, level, item):
        self.mob.add_behavior(Panic(10))

    def wound(self, damage=0):
        self.mob.add_behavior(Panic(10 + damage))


@Behavior.register
class Rage(Behavior):
    priority = 101

    def __init__(self, angry=None):
        super(Rage, self).__init__()
        self.angry = angry

    def act(self, level):
        if self.angry is not None:
            self.angry  -= 1
            if self.angry <= 0:
                self.mob.del_behavior(self)
        possible = [d for d in xrange(8)
                    if not level.is_blocked(dpos(self.mob.pos, d))]
        if possible:
            random.shuffle(possible)
            possible.sort(key = lambda d: dist2(dpos(self.mob.pos, d),
                                           level.player.pos))
            d = possible[0]
            self.mob.do_walk(level, d)
            return True

    def save(self):
        data = super(Rage, self).save()
        data['angry'] = self.angry
        return data

    def load(self, data):
        super(Rage, self).load(data)
        self.angry = data['angry']


@Behavior.register
class Hunting(Rage):
    priority = 0


@Behavior.register
class Panic(Behavior):
    priority = 152

    def __init__(self, afraid=None):
        super(Panic, self).__init__()
        self.afraid = afraid

    def act(self, level):
        if self.afraid is not None:
            self.afraid  -= 1
            if self.afraid <= 0:
                self.mob.del_behavior(self)
        possible = [d for d in xrange(8)
                    if not level.is_blocked(dpos(self.mob.pos, d))]
        if possible:
            random.shuffle(possible)
            possible.sort(key = lambda d: dist2(dpos(self.mob.pos, d),
                                           level.player.pos))
            d = possible[-1]
            self.mob.do_walk(level, d)
            return True

    def save(self):
        data = super(Panic, self).save()
        data['afraid'] = self.afraid
        return data

    def load(self, data):
        super(Panic, self).load(data)
        self.afraid = data['afraid']


@Behavior.register
class Running(Behavior):
    def __init__(self):
        super(Running, self).__init__()
        self.face = random.randint(0, 4)
        self.turn = random.choice([-1, 1])

    def act(self, level):
        count = 0
        while level.is_blocked(dpos(self.mob.pos, self.face)):
            self.face += self.turn
            count += 1
            if count >= 7:
                return False
        self.mob.do_walk(level, self.face)
        return True

    def save(self):
        data = super(Running, self).save()
        data['face'] = self.face
        data['turn'] = self.turn
        return data

    def load(self, data):
        super(Running, self).load(data)
        self.face = data['face']
        self.turn = data['turn']

@Behavior.register
class Patrol(Behavior):
    def __init__(self):
        super(Patrol, self).__init__()
        self.face = random.randint(0, 4)
        self.turn = random.choice([-1, 1])
        self.last_possible = 8

    def act(self, level):
        possible = [d for d in xrange(8)
                    if not level.is_blocked(dpos(self.mob.pos, d))]
        if not possible:
            return False
        if self.last_possible < 8:
            # Hug the wall
            side = (self.face + self.turn) % 8
            if side in possible:
                self.face = side
        self.last_possible = len(possible)
        while self.face not in possible:
            self.face = (self.face - self.turn) % 8
        self.mob.do_walk(level, self.face)
        return True

    def save(self):
        data = super(Patrol, self).save()
        data['face'] = self.face
        data['turn'] = self.turn
        data['last_possible'] = self.last_possible
        return data

    def load(self, data):
        super(Patrol, self).load(data)
        self.face = data['face']
        self.turn = data['turn']
        self.last_possible = data['last_possible']


@Behavior.register
class Thief(Behavior):
    priority = 151

    def act(self, level):
        if self.mob.item:
            self.mob.del_behavior(self)
            return
        for d in xrange(8):
            if level.player.pos == dpos(self.mob.pos, d):
                self.mob.do_steal(level, d, level.player)
                if self.mob.item:
                    self.mob.add_behavior(Panic())
                return True
