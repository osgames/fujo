#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

from . import things
from . import behaviors
from .util import DX, DY


class Item(things.Thing):
    action = "Use"
    looks = 1
    is_item = True

    def __init__(self):
        super(Item, self).__init__()
        self.animation = None

    def update(self, display):
        if not self.animation:
            return
        try:
            self.animation.next()
        except StopIteration:
            self.animation = None

    def pickup_anim(self):
        dy = 0
        image = self.image
        self.image = self.image.copy()
        while dy >= -4:
            dy -= 1
            self.rect.move_ip(0, dy)
            self.image.set_alpha(255 + 50 * dy)
            yield
        self.image = image
        self.kill()

    def throw_anim(self, pos, steps, direction, drop=False):
        self.rect.midbottom = 24 * pos[0], 16 * pos[1] - 3
        for step in xrange(steps):
            for frame in xrange(2):
                self.rect.move_ip(DX[direction] * 12, DY[direction] * 8)
                self.depth = self.rect.bottom + 8
                yield
        self.rect.midbottom = 24 * self.pos[0], 16 * self.pos[1] + 5
        self.depth = self.rect.bottom
        if not drop:
            self.kill()

    def pick_up(self, level):
        self.animation = self.pickup_anim()

    def get_name(self):
        return self.name

    def use(self, level):
        return True

    def throw(self, level, pos, mob):
        if mob:
            return True


def _calm_mob(mob):
    # TODO Find a nicer way of doing this.
    for b in mob.behaviors:
        if hasattr(b, 'angry'):
            mob.del_behavior(b)


class Food(Item):
    name = 'food'
    nutrition = 50
    action = "Eat"

    def use(self, level):
        super(Food, self).use(level)
        level.player.food = min(
            level.player.food + self.nutrition, level.player.max_food)
        level.player.health = min(
            level.player.health + self.nutrition // 10,
            level.player.max_health)
        level.player.label("%d" % (self.nutrition //10), (0x22, 0xff, 0x00))
        return True

    def throw(self, level, pos, mob):
        super(Food, self).throw(level, pos, mob)
        # Throwing food at monsters calms them down.
        if mob:
            _calm_mob(mob)
            return True

@Item.register
class Onigiri(Food):
    name = "Rice ball"
    nutrition = 75
    looks = 9

@Item.register
class Bento(Food):
    name = "Lunch box"
    nutrition = 125
    looks = 10

@Item.register
class Candy(Food):
    name = "Candy"
    nutrition = 50
    looks = 11


class Weapon(Item):
    name = 'weapon'
    attack = 3
    damage = 3
    action = "Wield/Unwield"
    looks = 12

    def __init__(self):
        super(Weapon, self).__init__()
        self.bonus = 0

    def get_name(self):
        if self.bonus:
            return '%s %+d' % (self.name, self.bonus)
        return self.name

    def use(self, level):
        super(Weapon, self).use(level)
        if level.player.weapon == self:
            level.player.equip_weapon(None)
        else:
            level.player.equip_weapon(self)
        # Equipping doesn't discard.
        return False

    def throw(self, level, pos, mob):
        super(Weapon, self).throw(level, pos, mob)
        if mob:
            mob.wound(self.damage * 2)
            return True

    def save(self):
        data = super(Weapon, self).save()
        data['bonus'] = self.bonus
        return data

    def load(self, data):
        super(Weapon, self).load(data)
        self.bonus = data['bonus']


@Item.register
class Broom(Weapon):
    name = "Broom"
    attack = 2
    damage = 2
    looks = 16

@Item.register
class Staff(Weapon):
    name = "Staff"
    attack = 4
    damage = 2
    looks = 14

@Item.register
class Sickle(Weapon):
    name = "Sickle"
    attack = 2
    damage = 4
    looks = 17

@Item.register
class Hammer(Weapon):
    name = "Hammer"
    attack = 6
    damage = 1
    looks = 15

@Item.register
class Hoe(Weapon):
    name = "Hoe"
    attack = 3
    damage = 3
    looks = 18

@Item.register
class Katana(Weapon):
    name = "Sword"
    attack = 3
    damage = 6
    looks = 19


class Spell(Item):
    identified = False
    action = "Apply"
    looks = 8

    def get_name(self):
        if self.identified:
            return self.name
        return "Unknown spell"

    def use(self, level):
        super(Spell, self).use(level)
        self.__class__.identified = True
        level.player.label(self.name)

    def throw(self, level, pos, mob):
        super(Spell, self).throw(level, pos, mob)
        level.player.label(self.name)
        if mob:
            self.__class__.identified = True
            return True

    def save(self):
        data = super(Spell, self).save()
        data['identified'] = self.identified
        return data

    def load(self, data):
        super(Spell, self).load(data)
        if data['identified'] != self.identified:
            self.identified = data['identified']


@Item.register
class FireSpell(Spell):
    name = "Fire spell"

    def use(self, level):
        super(FireSpell, self).use(level)
        level.player.wound(random.randint(3, 8))
        return True

    def throw(self, level, pos, mob):
        super(FireSpell, self).throw(level, pos, mob)
        if mob:
            mob.wound(random.randint(1, 5))
            mob.add_behavior(behaviors.Panic(10))
            level.player.smoke(mob.rect.midbottom, 6)
            return True


@Item.register
class TeleportSpell(Spell):
    name = "Teleport spell"

    def use(self, level):
        super(TeleportSpell, self).use(level)
        for pos in level.empty:
            if not level.is_blocked(pos):
                break
        else:
            return False
        del level.mobs[level.player.pos]
        level.player.smoke(None, 8)
        level.player.pos = None
        level.place_mob(level.player, pos)
        level.player.smoke(None, 8)
        return True

    def throw(self, level, pos, mob):
        super(TeleportSpell, self).throw(level, pos, mob)
        if mob:
            mob.add_behavior(behaviors.Confused(10))
            for pos in level.empty:
                if not level.is_blocked(pos):
                    break
            else:
                return
            level.player.smoke(mob.rect.midbottom, 8)
            del level.mobs[mob.pos]
            mob.pos = None
            level.place_mob(mob, pos)
            level.player.smoke(mob.rect.midbottom, 8)
            return True

@Item.register
class KnowledgeSpell(Spell):
    name = "Knowledge spell"

    def use(self, level):
        super(KnowledgeSpell, self).use(level)
        for item in level.player.items:
            item.__class__.identified = True
        return True

    def throw(self, level, pos, mob):
        super(KnowledgeSpell, self).throw(level, pos, mob)
        if mob:
            mob.add_behavior(behaviors.Confused(10))
            level.player.smoke(mob.rect.midbottom, 6)
        else:
            for row in level.explored:
                row[:] = [1] * len(row)
        level.explored[level.player.pos[1]][level.player.pos[0]] = 0
        self.__class__.identified = True
        return True

@Item.register
class SoothingSpell(Spell):
    name = "Soothing spell"

    def use(self, level):
        super(SoothingSpell, self).use(level)
        healing = level.player.max_health - level.player.health
        if healing <= 0:
            level.player.max_health += 1
            healing = 1
        level.player.health = level.player.max_health
        level.player.label("%d" % healing, (0x22, 0xff, 0x00))
        return True

    def throw(self, level, pos, mob):
        super(SoothingSpell, self).throw(level, pos, mob)
        if mob:
            healing = mob.max_health - mob.health
            mob.health = mob.max_health
            _calm_mob(mob)
            mob.add_behavior(behaviors.Sleep(10))
            mob.label("%d" % healing, (0x22, 0xff, 0x00))
            return True


@Item.register
class TransformSpell(Spell):
    name = "Transform spell"

    def use(self, level):
        super(TransformSpell, self).use(level)
        if level.player.weapon:
            i = level.player.items.index(level.player.weapon)
            level.player.equip_weapon(None)
            level.player.items[i] = Onigiri()
            return True
        return False

    def throw(self, level, pos, mob):
        super(TransformSpell, self).throw(level, pos, mob)
        if mob:
            mob.add_behavior(behaviors.Hide())
            level.player.smoke(mob.rect.midbottom, 12)
            return True


@Item.register
class SharpenSpell(Spell):
    name = "Sharpen spell"

    def use(self, level):
        super(SharpenSpell, self).use(level)
        if level.player.weapon:
            level.player.weapon.bonus += random.randint(1, 4)
            level.player.update_status()
            return True
        return False

    def throw(self, level, pos, mob):
        super(SharpenSpell, self).throw(level, pos, mob)
        for mob in level.mobs.values():
            if mob != level.player:
                mob.wound(0)
        self.__class__.identified = True
        return True


class Armor(Item):
    name = "armor"
    defense = 1
    action = "Wear/Remove"
    looks = 13

    def use(self, level):
        super(Armor, self).use(level)
        if level.player.armor == self:
            level.player.equip_armor(None)
        else:
            level.player.equip_armor(self)
        return False

    def throw(self, level, pos, mob):
        super(Armor, self).throw(level, pos, mob)
        if mob:
            _calm_mob(mob)
            mob.add_behavior(behaviors.Sleep(10))
            level.player.smoke(mob.rect.midbottom, 6)
            return True

@Item.register
class LuckyCharm(Armor):
    name = "Lucky charm"
    defense = 3

@Item.register
class Talisman(Armor):
    name = "Talisman"
    defense = 4

@Item.register
class Amulet(Armor):
    name = "Amulet"
    defense = 5


FOOD = [Onigiri, Bento, Candy]
WEAPONS = [Broom, Staff, Sickle, Hoe, Hammer, Katana]
SPELLS = [FireSpell, TeleportSpell, KnowledgeSpell, SoothingSpell,
          TransformSpell, SharpenSpell]
ARMOR = [LuckyCharm, Talisman, Amulet]
ITEMS = FOOD + WEAPONS + SPELLS + ARMOR
