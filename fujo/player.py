#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

import pygame

from . import items
from . import things
from . import window
from . import mobs
from .util import DX, DY, COLOR_RED, COLOR_GREEN, dpos
from .resources import RES


DIR_KEYS = [
    (1, pygame.K_DOWN, pygame.K_LEFT),
    (3, pygame.K_LEFT, pygame.K_UP),
    (5, pygame.K_UP, pygame.K_RIGHT),
    (7, pygame.K_RIGHT, pygame.K_DOWN),

    (0, pygame.K_DOWN, None),
    (2, pygame.K_LEFT, None),
    (4, pygame.K_UP, None),
    (6, pygame.K_RIGHT, None),

    (0, pygame.K_j, None),
    (2, pygame.K_h, None),
    (4, pygame.K_k, None),
    (6, pygame.K_l, None),

    (1, pygame.K_b, None),
    (3, pygame.K_y, None),
    (5, pygame.K_u, None),
    (7, pygame.K_n, None),
]


@mobs.Mob.register
class Player(mobs.Mob):
    is_player = True

    def __init__(self):
        self.max_health = 20
        super(Player, self).__init__(RES.player)
        self.explored = 0
        self.next_heal = 100
        self.face = 0
        self.animation = None
        self.level = 0
        self.status_window = None
        self.max_food = 200
        self.food = 100
        self.hunger = 0
        self.dead = False
        self.climb = False
        self.weapon = None
        self.armor = None
        weapon = items.Broom()
        self.equip_weapon(weapon)
        spell = random.choice(items.SPELLS)()
        spell.identified = True  # Only identify this one instance
        self.items = [
            weapon,
            items.Bento(),
            spell,
        ]
        self.new_sprites = []
        self.new_things = []

    @property
    def attack(self):
        if self.weapon:
            return self.weapon.attack + self.weapon.bonus
        return 1

    @property
    def damage(self):
        if self.weapon:
            return self.weapon.damage + self.weapon.bonus
        return 1

    @property
    def defense(self):
        if self.armor:
            return self.armor.defense
        return 2

    def metabolize(self, level):
        if self.food:
            self.food -= 1
            self.hunger = 0
        else:
            self.hunger += 1
            self.label("Hungry", COLOR_RED)
            if self.hunger > 5:
                self.health -= 1
                self.label("1", COLOR_RED, 4)
                self.hunger = 0
                self.update_status()
        if self.explored > self.next_heal:
            self.next_heal += 100
            self.health = min(self.max_health, self.health + 1)
            self.label("1", COLOR_GREEN, 4)
            self.update_status()
        if self.health <= 0:
            self.die()

    def die(self):
        self.dead = True
        self.animation = self.die_anim()
        self.label("Dead", COLOR_RED)

    def equip_weapon(self, item):
        self.weapon = item
        self.update_status()

    def equip_armor(self, item):
        self.armor = item
        self.update_status()

    def discard_item(self, item):
        self.items.remove(item)
        if self.weapon == item:
            self.equip_weapon(None)
        if self.armor == item:
            self.equip_armor(None)
        return False

    def pick_item(self, level):
        thing = level.things.get(self.pos)
        if thing and thing.is_item:
            item = level.pick_item(thing)
            self.label(item.get_name(), delay=2, place=thing.rect.midtop)
            self.items.append(item)

    def act(self, level, display):
        if self.dead:
            return True
        keys = pygame.key.get_pressed()
        for direction, k1, k2 in DIR_KEYS:
            if keys[k1] and (k2 is None or keys[k2]):
                break
        else:
            direction = None
        if direction is not None:
            self.face = direction
            if keys[pygame.K_LCTRL]:
                return False
            pos = dpos(self.pos, direction)
            mob = level.mobs.get(pos)
            if mob:
                level.attack_mob(self, mob)
                self.animation = self.attack_anim(direction)
                return True
            elif not level.walls[pos[1]][pos[0]]:
                level.place_mob(self, pos)
                self.animation = self.walk_anim(direction)
                self.pick_item(level)
                return True
        elif keys[pygame.K_SPACE]:
            self.animation = self.walk_anim()
            return True
        elif keys[pygame.K_RETURN]:
            return self.inventory(level, display)
        return False

    def smoke(self, place, num=4):
        if place is None:
            place = self.rect.move(0, -4).midbottom
        for i in range(num):
            puff = things.Puff()
            puff.rect.center = place
            self.new_sprites.append(puff)

    def inventory(self, level, display):
        menu = window.Menu((display.size[0] - 196, 4, 192, 32),
            [(
                item,
                item.get_name() +
                    (" (in hand)" if item == self.weapon else '') +
                    (" (worn)" if item == self.armor else '')
                ) for item in self.items])
        while True:
            item = menu.run(display)
            if item:
                submenu = window.Menu((display.size[0] - 200, 36, 96, 32), [
                    ('use', item.action),
                    ('throw', "Throw"),
                    ('drop', "Drop"),
                ], 2)
                action = submenu.run(display)
                submenu.kill()
                if action:
                    break
            else:
                break
        menu.kill()
        if not item or not action:
            return False
        elif action == 'drop':
            self.discard_item(item)
            level.drop_item(self.pos, item)
        elif action == 'use':
            if item.use(level):
                self.discard_item(item)
            self.update_status()
        elif action == 'throw':
            level.throw_item(self, item, self.face)
            self.discard_item(item)
            display.spawn_thing(item)
            self.animation = self.throw_anim(self.face, item)
        return True

    def update_status(self):
        if self.status_window is None:
            return
        self.status_window.set_lines([
            "Health %d/%d  Attack %d  Defense %d  Damage %d" % (
                self.health, self.max_health, self.attack,
                self.defense, self.damage
            )
        ])

    def wound(self, damage=1):
        if damage:
            self.health -= damage
            self.update_status()
            self.label('%d' % damage, COLOR_RED, 4)
        if self.health <= 0:
            self.die()

    def update(self, display):
        super(Player, self).update(display)
        for thing in self.new_things:
            display.spawn_thing(thing)
        self.new_things[:] = []
        for sprite in self.new_sprites:
            display.sprites.add(sprite)
        self.new_sprites[:] = []
        if self.animation:
            try:
                self.animation.next()
            except StopIteration:
                self.animation = None
        else:
            self.image = self.frames[self.face]

    def walk_anim(self, direction=None):
        for frame in xrange(4):
            for step in xrange(2):
                yield
                if direction is not None:
                    self.rect.move_ip(3 * DX[direction], 2 * DY[direction])
                self.depth = self.rect.bottom + 1
            self.image = self.frames[self.face + frame * 8]

    def attack_anim(self, direction):
        self.image = self.frames[self.face + 1]
        for frame in xrange(3):
            self.rect.move_ip(3 * DX[direction], 2 * DY[direction])
            self.depth = self.rect.bottom + 1
            yield
        for frame in xrange(3):
            self.rect.move_ip(-3 * DX[direction], -2 * DY[direction])
            self.depth = self.rect.bottom + 1
            yield
        yield
        yield

    def throw_anim(self, direction, item):
        self.image = self.frames[self.face + 1]
        for frame in xrange(3):
            self.rect.move_ip(3 * DX[direction], 2 * DY[direction])
            self.depth = self.rect.bottom + 1
            yield
        for frame in xrange(3):
            self.rect.move_ip(-3 * DX[direction], -2 * DY[direction])
            self.depth = self.rect.bottom + 1
            yield
        while item.animation:
            yield

    def die_anim(self):
        for count in xrange(4):
            for frame in xrange(8):
                self.image = self.frames[frame]
                yield
        self.kill()

    def climb_anim(self):
        for count in xrange(2):
            for frame in xrange(4):
                for step in xrange(2):
                    yield
                    self.rect.move_ip(0, -1)
                self.image = self.frames[4 + frame * 8]
        self.climb = True

    def save(self):
        data = super(Player, self).save()
        data.update({
            'health': self.health,
            'max_health': self.max_health,
            'food': self.food,
            'max_food': self.max_food,
            'hunger': self.hunger,
            'level': self.level,
            'weapon': self.items.index(self.weapon) if self.weapon else None,
            'armor': self.items.index(self.armor) if self.armor else None,
            'items': [item.save() for item in self.items],
            'face': self.face,
            'explored': self.explored,
            'next_heal': self.next_heal,
        })
        return data

    def load(self, data):
        super(Player, self).load(data)
        self.health = data['health']
        self.max_health = data['max_health']
        self.food = data['food']
        self.max_food = data['max_food']
        self.hunger = data['hunger']
        self.level = data['level']
        self.items = [items.Item.create(item) for item in data['items']]
        self.weapon = self.items[data['weapon']] if data['weapon'] else None
        self.armor = self.items[data['armor']] if data['armor'] else None
        self.face = data['face']
        self.explored = data['explored']
        self.next_heal = data['next_heal']
