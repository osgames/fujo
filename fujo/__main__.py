#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame

from . import title
from .resources import RES


MODE = 320, 200


pygame.display.init()
pygame.font.init()
try:
    pygame.mixer.init()
except Exception:
    pass
pygame.display.set_mode(MODE)
pygame.display.set_caption("Fujo")
RES.load()
title.Title().run()
pygame.quit()
