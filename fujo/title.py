#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

import pygame

from . import window
from . import display
from . import game
from . import saves
from .resources import RES


HINTS = [
    "You can hold down CTRL to rotate in place without walking.",
    "If you press SPACE, you will wait one turn without moving.",
    "Throwing items at monsters can give interesting results.",
    "Tanukis can hide by transforming into various things.",
    "Make sure to equip your weapons and amulets to make use of them.",
    "Bump into monsters to attack them.",
    "You always throw in the direction you are currently facing.",
    "Many monsters will not attack you unless you anger them.",
    "Monsters can get angry when they see you taking their items.",
    "Sightseeing can be very therapeutic.",
    "Once you identify a spell, you will know when you find another like it.",
    "You lose the items when you throw them.",
]


class Title(object):
    def __init__(self):
        self.screen = pygame.display.get_surface()
        self.windows = display.Sprites()
        self.clock = pygame.time.Clock()
        self.quit = False

    def tick(self):
        self.clock.tick(20)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.quit = True

    def update(self):
        self.windows.clear(self.screen, RES.title)
        self.windows.update()
        dirty = self.windows.draw(self.screen)
        pygame.display.update(dirty)

    def run(self):
        options = [
            ('continue', 'Continue'),
            ('start', 'Start new game'),
            ('music off', 'Turn music off'),
            ('exit', 'Exit'),
        ]
        if pygame.mixer.get_init():
            RES.load_music()
            pygame.mixer.music.play(-1)
        else:
            del options[2]
        hint = window.Message((0, -3, 480, 32), ['hint'])
        self.windows.add(hint)
        menu = window.Menu((272, 64, 160, 32), options)
        self.screen.blit(RES.title, (0, 0))
        pygame.display.flip()
        while True:
            hint.set_lines([random.choice(HINTS)])
            if not saves.valid():
                options[0] = ('', ' ')
                if menu.pos == 0:
                    menu.pos = 1
            menu.set_lines(label for choice, label in options)
            choice = menu.run(self)
            if choice == 'start':
                run = True
                while run:
                    run = game.Game().run()
                self.screen.blit(RES.title, (0, 0))
                pygame.display.flip()
            elif choice == 'continue':
                run = game.Game().run(saves.load())
                while run:
                    run = game.Game().run()
                self.screen.blit(RES.title, (0, 0))
                pygame.display.flip()
            elif choice == 'music off':
                pygame.mixer.music.stop()
                options[2] = ('music on', 'Turn music on')
            elif choice == 'music on':
                pygame.mixer.music.play(-1)
                options[2] = ('music off', 'Turn music off')
            elif choice in ('exit', None):
                return
