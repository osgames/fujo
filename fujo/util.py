#!/usr/bin/env python
# -*- coding: utf-8 -*-

DX = [0, -1, -1, -1,  0,  1, 1, 1]
DY = [1,  1,  0, -1, -1, -1, 0, 1]

COLOR_RED = (0xff, 0x00, 0x00)
COLOR_WHITE = (0xee, 0xff, 0xff)
COLOR_ORANGE = (0xff, 0xaa, 0x00)
COLOR_GREEN = (0x22, 0xff, 0x00)


def dpos(pos, direction):
    return pos[0] + DX[direction], pos[1] + DY[direction]


def dist((x1, y1), (x2, y2)):
    return max(abs(x2 - x1), abs(y2 - y1))


def sum_dist(pos1, pos2):
    return abs(pos1[0] - pos2[0]) + abs(pos1[1] - pos2[1])


def dist2(pos1, pos2):
    return dist(pos1, pos2) + sum_dist(pos1, pos2)
