#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame

from . import display
from . import level
from . import player
from . import window
from . import saves


class Game(object):
    def __init__(self):
        self.display = display.Display()
        self.status_window = window.Message(
            (1, self.display.size[1] - 29, 272, 32), [])
        self.display.windows.add(self.status_window)
        self.player = None


    def new_level(self, next_level=None):
        if next_level is None:
            if self.player is None:
                self.player = player.Player()
            else:
                self.player.level += 1
            if self.player.level > 5:
                self.running = False
                return
            self.player.pos = None
            self.player.climb = False
            self.level = level.BasicLevel(self.player)
            self.level.generate()
            self.level.populate()
            self.level.explore(self.player.pos)
            saves.save(self.level.save())
        else:
            self.level = next_level
            self.player = self.level.player
        self.player.status_window = self.status_window
        self.player.update_status()
        self.display.render_level(self.level)
        self.center()
        self.display.refresh()

    def center(self):
        camera = self.display.camera
        r = self.display.screen.get_rect()
        camera.center = self.player.rect.center
        if camera.top < 0:
            camera.top = 0
        if camera.left < 0:
            camera.left = 0
        if camera.right > r.width:
            camera.right = r.width
        if camera.bottom > r.height:
            camera.bottom = r.height


    def tick(self):
        if pygame.key.get_pressed()[pygame.K_ESCAPE]:
            menu = window.Menu((256, 64, 80, 32), [
                ('continue', 'Continue'),
                ('quit', 'Quit'),
            ])
            self.running = menu.run(self.display) in (None, 'continue')
            menu.kill()
        elif self.player.animation is None:
            margin = 48
            if not self.display.camera.inflate(-margin, -margin).contains(
                    self.player.rect):
                self.center()
                self.display.refresh()
            if self.player.dead:
                self.running = False
                return
            if self.display.quit:
                saves.save(self.level.save())
                pygame.quit()
                raise SystemExit()
            if self.player.climb:
                self.new_level()
                return
            if getattr(self.level.things.get(self.player.pos), 'is_exit', 0):
                self.player.animation = self.player.climb_anim()
                return
            acted = self.player.act(self.level, self.display)
            self.player.update(self.display)
            if acted:
                explored = self.level.explore(self.player.pos)
                if explored:
                    self.player.explored += explored
                    self.display.update_explored(self.level)
                    self.display.refresh()
                for mob in self.level.mobs.values():
                    mob.metabolize(self.level)
                for mob in self.level.mobs.values():
                    if mob != self.player:
                        mob.act(self.level)

    def run(self, load_data=None):
        self.running = True
        if load_data:
            next_level = level.Level.create(load_data)
        else:
            next_level = None
        self.new_level(next_level)
        while self.running:
            self.tick()
            self.display.update()
            self.display.tick()
        saves.delete()
        options = [
            ('again', 'Play again'),
            ('exit', 'Exit to title'),
        ]
        menu = window.Menu((256, 64, 80, 32), options)
        choice = menu.run(self.display)
        return choice == 'again'

