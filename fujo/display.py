#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame

from .resources import RES
from . import things


WALL_LOOKUP = [None, 14, 10, 13, 4, 9, 1, 12, 0, 2, 5, 11, 3, 6, 7, 8]


def render_layer(surface, layer_map, atlas, is_wall, dy=0, blit_flags=0):
    for y, (row1, row2) in enumerate(zip(layer_map, layer_map[1:])):
        for x, cells in enumerate(zip(row1, row1[1:], row2, row2[1:])):
            tile = WALL_LOOKUP[sum(2 ** i for (i, c) in
                                   enumerate(cells) if is_wall(c))]
            if tile is not None:
                surface.blit(atlas[tile], (x * 24, y * 16 + dy),
                            special_flags=blit_flags)


class Shadows(pygame.sprite.Group):
    def draw(self, surface):
       spritedict = self.spritedict
       surface_blit = surface.blit
       dirty = self.lostsprites
       self.lostsprites = []
       dirty_append = dirty.append
       for s in self.sprites():
           r = spritedict[s]
           newrect = surface_blit(s.image, s.rect,
                                  special_flags=pygame.BLEND_MULT)
           if r is 0:
               dirty_append(newrect)
           else:
               if newrect.colliderect(r):
                   dirty_append(newrect.union(r))
               else:
                   dirty_append(newrect)
                   dirty_append(r)
           spritedict[s] = newrect
       return dirty


class Sprites(pygame.sprite.RenderUpdates):
    def sprites(self):
        return sorted(self.spritedict.keys(),
                      key=lambda sprite: getattr(sprite, "depth", 0))


class Display(object):
    def __init__(self):
        self.real_screen = pygame.display.get_surface()
        self.screen_size = (self.real_screen.get_width(),
                          self.real_screen.get_height())
        self.size = 480, 480
        self.scale = 1
        self.screen = pygame.Surface(self.size)
        self.windows = Sprites()
        self.labels = Sprites()
        self.clock = pygame.time.Clock()
        self.quit = False
        self.camera = pygame.Rect(
            0, 0,
            self.screen_size[0] / self.scale,
            self.screen_size[1] / self.scale)

    def tick(self):
        self.clock.tick(24)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.quit = True

    def update(self):
        self.windows.clear(self.real_screen, self.windows_background)
        self.labels.clear(self.screen, self.clear_background)
        self.sprites.clear(self.screen, self.clear_background)
        self.shadows.clear(self.screen, self.clear_background)
        self.sprites.update(self)
        self.labels.update()
        self.shadows.update()
        self.windows.update()
        dirty_shadows = self.shadows.draw(self.screen)
        dirty = self.sprites.draw(self.screen)
        for rect in dirty:
            self.screen.blit(self.overlay, rect, rect)
            self.screen.blit(self.fog, rect, rect,
                             special_flags=pygame.BLEND_MULT)
        dirty_labels = self.labels.draw(self.screen)
        dirty.extend(dirty_shadows)
        dirty.extend(dirty_labels)
        view = self.screen.subsurface(self.camera)
        pygame.transform.scale(view, self.screen_size, self.real_screen)
        real_dirty = self.windows.draw(self.real_screen)
        for rect in dirty:
            rect = rect.move(-self.camera.x, -self.camera.y)
            real_rect = pygame.Rect(
                rect.x * self.scale, rect.y * self.scale,
                rect.width * self.scale, rect.height * self.scale)
            real_dirty.append(real_rect)
        pygame.display.update(real_dirty)

    def refresh(self):
        self.clear_background.blit(self.background, (0, 0))
        self.clear_background.blit(self.fog, (0, 0),
                                   special_flags=pygame.BLEND_MULT)
        view = self.clear_background.subsurface(self.camera)
        pygame.transform.scale(view, self.screen_size, self.windows_background)

        self.screen.blit(self.background, (0, 0))
        self.shadows.update()
        self.shadows.draw(self.screen)
        self.sprites.draw(self.screen)
        self.screen.blit(self.overlay, (0, 0))
        self.screen.blit(self.fog, (0, 0), special_flags=pygame.BLEND_MULT)
        view = self.screen.subsurface(self.camera)
        pygame.transform.scale(view, self.screen_size, self.real_screen)
        self.windows.draw(self.real_screen)
        pygame.display.flip()

    def spawn_mob(self, mob):
        if mob not in self.sprites:
            self.sprites.add(mob)
            self.shadows.add(things.Shadow(mob))

    def spawn_thing(self, thing):
        if thing not in self.sprites:
            self.sprites.add(thing)
            self.shadows.add(things.Shadow(thing, 1))

    def render_level(self, level):
        self.windows_background = pygame.Surface(self.screen_size)
        self.clear_background = pygame.Surface(self.size)
        self.background = pygame.Surface(self.size)
        self.overlay = pygame.Surface(self.size, pygame.SRCALPHA)
        self.fog = pygame.Surface(self.size)
        self.shadows = Shadows()
        self.sprites = Sprites()
        for y, row in enumerate(level.floor):
            for x, cell in enumerate(row):
                self.background.blit(RES.floor_tiles[cell],
                                     (x * 24 - 12, y * 16 - 8))
        render_layer(self.background, level.walls, RES.wall_tiles[0:],
                     lambda c: c == 0, 0, pygame.BLEND_MULT)
        render_layer(self.overlay, level.walls,
                     RES.wall_tiles[30 * level.looks + 15:],
                     lambda c: c != 0, -8)
        render_layer(self.background, level.walls,
                     RES.wall_tiles[30 * level.looks + 30:],
                     lambda c: c != 0, -8)
        for mob in level.mobs.values():
            self.spawn_mob(mob)
        for thing in level.things.values():
            self.spawn_thing(thing)
        self.update_explored(level)

    def update_explored(self, level):
        self.fog.fill((0xff, 0xff, 0xff))
        render_layer(self.fog, level.explored, RES.wall_tiles[75:],
                     lambda c: c == 0, 0)
