#!/usr/bin/env python
# -*- coding: utf-8 -*-

import weakref
import math
import random

import pygame

from .resources import RES
from . import saves


class Shadow(pygame.sprite.Sprite):
    def __init__(self, sprite, looks=0):
        super(Shadow, self).__init__()
        self.image = RES.things[looks]
        self.rect = self.image.get_rect()
        self.sprite = weakref.ref(sprite)

    def update(self):
        sprite = self.sprite()
        if sprite and sprite.alive():
            self.rect.midbottom = sprite.rect.midbottom
            try:
                self.rect.bottom = sprite.depth
            except AttributeError:
                pass
        else:
            self.kill()

class Puff(pygame.sprite.Sprite):
    def __init__(self):
        super(Puff, self).__init__()
        self.frames = RES.things[24:]
        self.image = self.frames[0]
        self.rect = self.image.get_rect()
        self.animation = self.default_anim()

    def default_anim(self):
        self.depth = self.rect.bottom + 2
        dx = random.randint(-4, 5)
        dy = -random.randint(0, 4)
        frame = 0
        for step in range(24):
            if random.choice([0, 1]):
                frame += 1
                if frame > 6:
                    frame = 3
                self.image = self.frames[frame].copy()
            self.image.set_alpha(255 - step * 10)
            if dx and step % abs(dx) == 0:
                self.rect.move_ip(math.copysign(1, dx), 0)
            if dy and step % abs(dy) == 0:
                self.rect.move_ip(0, math.copysign(1, dy))
            yield


    def update(self, display):
        try:
            self.animation.next()
        except StopIteration:
            self.kill()


class Thing(pygame.sprite.Sprite, saves.Saveable):
    is_exit = False
    is_item = False
    kinds = {}

    def __init__(self):
        super(Thing, self).__init__()
        self.image = RES.things[self.looks]
        self.rect = self.image.get_rect()
        self.pos = None

    def set_pos(self, pos):
        if self.pos is None:
            self.rect.midbottom = 24 * pos[0], 16 * pos[1] + 5
            self.depth = self.rect.bottom
        self.pos = pos

    def save(self):
        data = super(Thing, self).save()
        data['position'] = self.pos
        return data

    def load(self, data):
        super(Thing, self).load(data)
        self.pos = None
        if data['position']:
            self.set_pos(tuple(data['position']))


@Thing.register
class Ladder(Thing):
    looks = 2
    is_exit = True

@Thing.register
class Hole(Thing):
    looks = 3


@Thing.register
class SmallBush(Thing):
    looks = 4


@Thing.register
class Bush(Thing):
    looks = 5


@Thing.register
class Rocks(Thing):
    looks = 6


@Thing.register
class Stones(Thing):
    looks = 7


DEBRIS = [Bush, SmallBush, Rocks, Stones]
