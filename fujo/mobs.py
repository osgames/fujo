#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

import pygame

from .resources import RES
from . import window
from . import saves
from . import behaviors
from . import things
from .util import DX, DY, COLOR_ORANGE, COLOR_WHITE, dpos


class Mob(pygame.sprite.Sprite, saves.Saveable):
    is_player = False
    max_health = 0
    kinds = {}

    def __init__(self, frames):
        super(Mob, self).__init__()
        self.frames = frames
        self.image = frames[0]
        self.rect = self.image.get_rect()
        self.pos = None
        self.frame = 0
        self.health = self.max_health
        self.new_labels = []

    def set_pos(self, pos):
        if self.pos is None:
            self.rect.midbottom = 24 * pos[0], 16 * pos[1] + 5
        self.pos = pos
        self.depth = self.rect.bottom

    def update(self, display):
        for sprite in self.new_labels:
            display.labels.add(sprite)
        self.new_labels[:] =[]

    def label(self, text, color=COLOR_WHITE, delay=0, place=None):
        offset = 12 * len(self.new_labels)
        if place is None:
            place = self.rect.midtop
        self.new_labels.append(
            window.Label(place, text, color, delay, offset))

    def item_picked(self, level, thing):
        return

    def mob_attacked(self, level, mob, damage):
        return

    def save(self):
        data = super(Mob, self).save()
        data.update({
            'position': self.pos,
            'health': self.health,
        })
        return data

    def load(self, data):
        super(Mob, self).load(data)
        self.pos = None
        self.set_pos(tuple(data['position']))
        self.health = data['health']


class Monster(Mob):
    max_health = 5
    attack = 2
    damage = 2
    defense = 1
    looks = 0

    def __init__(self):
        super(Monster, self).__init__(RES.monsters)
        self.animation = self.walk_anim()
        self.behaviors = []
        self.item = None
        self.dead = False
        self.init_behaviors()

    def init_behaviors(self):
        self.add_behavior(behaviors.Sleep(random.randint(0, 5)))
        self.add_behavior(behaviors.Hostile())
        self.add_behavior(behaviors.Excitable())
        self.add_behavior(behaviors.Wander())

    def add_behavior(self, behavior):
        behavior.mob = self
        self.behaviors.append(behavior)
        self.behaviors.sort(key=lambda b: -b.priority)
        behavior.added()

    def del_behavior(self, behavior):
        self.behaviors.remove(behavior)

    def do_walk(self, level, direction=None):
        if direction is not None:
            level.place_mob(self, dpos(self.pos, direction))
        self.animation = self.walk_anim(direction)

    def do_attack(self, level, direction, mob):
        level.attack_mob(self, mob)
        self.animation = self.attack_anim(direction)

    def do_steal(self, level, direction, mob):
        if self.item:
            return
        self.item = level.mob_mob(mob)
        self.animation = self.attack_anim(direction)
        if self.item:
            self.label(self.item.name)

    def metabolize(self, level):
        if self.health <= 0:
            self.die(level)
            self.dead = True

    def die(self, level):
        del level.mobs[self.pos]
        if self.item:
            level.drop_item(self.pos, self.item)
        self.animation = self.death_anim()

    def item_picked(self, level, thing):
        for behavior in self.behaviors[:]:
            if behavior.item_picked(level, thing):
                break

    def mob_attacked(self, level, mob, damage):
        for behavior in self.behaviors[:]:
            if behavior.mob_attacked(level, mob, damage):
                break

    def item_thrown(self, level, item):
        for behavior in self.behaviors[:]:
            if behavior.item_thrown(level, item):
                break

    def act(self, level):
        for behavior in self.behaviors[:]:
            if behavior.act(level):
                break
        return True

    def wound(self, damage=0):
        for behavior in self.behaviors[:]:
            if behavior.wound(damage):
                break
        else:
            if damage:
                self.health -= damage
                self.label('%d' % damage, COLOR_ORANGE)

    def update(self, display):
        super(Monster, self).update(display)
        try:
            self.animation.next()
        except StopIteration:
            self.animation = self.walk_anim()
            self.animation.next()

    def walk_anim(self, direction=None):
        for frame in xrange(4):
            for step in xrange(2):
                if direction is not None:
                    self.rect.move_ip(3 * DX[direction], 2 * DY[direction])
                    self.depth = self.rect.bottom
                yield
            self.image = self.frames[frame * 8 + self.looks]

    def attack_anim(self, direction):
        self.image = self.frames[8 + self.looks]
        yield
        yield
        for frame in xrange(3):
            self.rect.move_ip(3 * DX[direction], 2 * DY[direction])
            self.depth = self.rect.bottom
            yield
        for frame in xrange(3):
            self.rect.move_ip(-3 * DX[direction], -2 * DY[direction])
            self.depth = self.rect.bottom
            yield

    def hide_anim(self):
        # Look like food or bush
        item = random.choice([4, 5, 9, 10, 11])
        self.image = RES.things[item]
        while True:
            yield

    def death_anim(self):
        size = 0
        for frame in xrange(8):
            self.image = pygame.transform.scale(
                self.frames[8 + self.looks], (32, 32 + size * 4)).convert()
            size += frame
            self.rect.move_ip(0, -frame * 4)
            self.image.set_colorkey((0x00, 0x00, 0x00))
            self.image.set_alpha(255 - frame * 30)
            yield
        self.kill()

    def save(self):
        data = super(Monster, self).save()
        data.update({
            'attack': self.attack,
            'damage': self.damage,
            'defense': self.defense,
            'behaviors': [b.save() for b in self.behaviors],
            'item': self.item.save if self.item else None,
        })
        return data

    def load(self, data):
        super(Monster, self).load(data)
        if data['attack'] != self.attack:
            self.attack = data['attack']
        if data['defense'] != self.defense:
            self.defense = data['defense']
        if data['damage'] != self.damage:
            self.damage = data['damage']
        self.item = things.Thing.create(data['item']) if data['item'] else None
        self.behaviors = []
        for b in data['behaviors']:
            self.add_behavior(behaviors.Behavior.create(b))


@Mob.register
class Martian(Monster):
    looks = 0

    def init_behaviors(self):
        self.add_behavior(behaviors.Sleep(random.randint(0, 5)))
        self.add_behavior(behaviors.Coward())
        self.add_behavior(behaviors.Hostile())
        self.add_behavior(behaviors.Hunting())


@Mob.register
class Boss(Monster):
    looks = 1
    attack = 4
    damage = 4
    defense = 3
    max_health = 20

    def metabolize(self, level):
        super(Boss, self).metabolize(level)
        if self.health <= 0:
            level.player.climb = True

    def init_behaviors(self):
        self.add_behavior(behaviors.Coward())
        self.add_behavior(behaviors.Hostile())
        self.add_behavior(behaviors.Hunting())


@Mob.register
class Tengu(Monster):
    looks = 2
    damage = 3
    attack = 4

    def init_behaviors(self):
        self.add_behavior(behaviors.Sleep(random.randint(0, 5)))
        self.add_behavior(behaviors.Hostile())
        self.add_behavior(behaviors.Patrol())


@Mob.register
class Kappa(Monster):
    looks = 3

    def init_behaviors(self):
        self.add_behavior(behaviors.Sleep(random.randint(0, 5)))
        self.add_behavior(behaviors.Thief())
        self.add_behavior(behaviors.Hostile())
        self.add_behavior(behaviors.Hunting())


@Mob.register
class Bat(Monster):
    looks = 4
    damage = 1
    defense = 0
    attack = 1
    max_health = 2

    def init_behaviors(self):
        self.add_behavior(behaviors.Hostile())
        self.add_behavior(behaviors.Wander())


@Mob.register
class Ghost(Monster):
    looks = 5

    def init_behaviors(self):
        self.add_behavior(behaviors.Hostile())
        self.add_behavior(behaviors.Excitable())
        self.add_behavior(behaviors.Wander())


@Mob.register
class Tanuki(Monster):
    looks = 6

    def init_behaviors(self):
        self.add_behavior(behaviors.Hostile())
        self.add_behavior(behaviors.Excitable())
        self.add_behavior(behaviors.Patrol())
        self.add_behavior(behaviors.Hide())


@Mob.register
class Oni(Monster):
    looks = 7

    def init_behaviors(self):
        self.add_behavior(behaviors.Sleep(random.randint(0, 5)))
        self.add_behavior(behaviors.Hostile())
        self.add_behavior(behaviors.Hunting())
