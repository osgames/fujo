#!/usr/bin/env python2

try:
    import pygame
except ImportError:
    raise SystemExit("You need to install PyGame.")
else:
    from fujo import __main__
