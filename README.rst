Fujo
****

This is a dungeon crawl game. You are a young temple maiden, who has been
kidnapped by monsters from her village. In the monster's cave, you overheard
the monsters planning to ambush and kill all the villagers. You have to do
something! You check all your belongings, but you only find a single random
spell on you. YOU GET ONLY ONE. No matter, you have to save the village and
yourself! Note that if you die, you are dead -- no way to go back to an earlier
checkpoint or savegame. You have to start the game from the beginning. Then
again, remember that you are expected to die a lot until you learn to play the
game, so don't get discouraged too fast.


Moving around
=============

You can move around using arrows. Please note, that you don't have to hurry --
the game is turn-based, and the monsters will only move when you do, so you can
go as slow (or as fast) as you want. In order to change your facing without
actually walking, hold down the Ctrl key when pressing the arrows. To wait in
place without moving, press the Space.

Combat
======

To attack a monster, simply walk into it. Note, that the monsters can and will
hit you back, so it may be wise not to attack blindly everything that moves.
You can also throw objects at the monsters, with a variety of effects.

Items
=====

You will find many different items that can help you along the way. You will
need to eat food regularly, or you will die of starvation. The food will also
heal you a little bit. You can use weapons for improving your offensive
capabilities, and amulets to improve your defense. Last, but not least, you can
use those small slips of paper with magic symbols on them. You can either apply
them to yourself, or throw them at the monsters. Most of them will start
unidentified -- you have to try them to see what they do. Once you have tried a
particular spell, you will see its name normally.
